"""
Definition of models.
"""

from django.db import models

# Create your models here.
class Post(models.Model):
        author = models.CharField(max_length=40)
        title = models.CharField(max_length=30)
        text = models.TextField()
        created_date = models.DateTimeField()
        published_date = models.DateTimeField()
        
class Users(models.Model):
        first_name = models.CharField(max_length=30)
        last_name = models.CharField(max_length=30)


